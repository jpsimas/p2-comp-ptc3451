set datafile separator ','
set terminal postscript eps color enhanced size 16cm,12cm

# periodogram of x1
# set terminal x11 0
set output "../img/periodX1.eps";
set xlabel "Discrete Frequency"
set ylabel "Amplitude (dB)"
set title ""

plot "../out/x1Periodogram.csv" using (10*log10(column(1))) w linesp lt rgb "red" pt "+" ps 10 notitle

# waveforms of x_i
# set terminal x11 1
set output "../img/wavX.eps";

set multiplot layout 2, 2
plot "../out/X.csv" using 1 title "x_1" lt rgb "blue" pt "+"
plot "../out/X.csv" using 2 title "x_2" lt rgb "blue" pt "+"
plot "../out/X.csv" using 3 title "x_3" lt rgb "blue" pt "+"
unset multiplot

# waveforms of y_i
# set terminal x11 2
set output "../img/wavY.eps";

set multiplot layout 2, 2
set yrange [-20:20]
plot "../out/Y.csv" using 1 title "y_1" lt rgb "blue" pt "+"
plot "../out/Y.csv" using 2 title "y_2" lt rgb "blue" pt "+"
plot "../out/Y.csv" using 3 title "y_3" lt rgb "blue" pt "+"
unset yrange
unset multiplot

# scatterplots of x_i
# set terminal x11 3
set output "../img/scatterX.eps";

set multiplot layout 2, 2
set xlabel "x_2"
set ylabel "x_1"
plot "../out/X.csv" using 2:1 lt rgb "blue" pt "+" notitle
set xlabel "x_3"
set ylabel "x_1"
plot "../out/X.csv" using 3:1 lt rgb "blue" pt "+" notitle
set xlabel "x_3"
set ylabel "x_2"
plot "../out/X.csv" using 3:2 lt rgb "blue" pt "+" notitle
unset xlabel
unset ylabel
unset multiplot

# histograms of x
# set terminal x11 4
set output "../img/histX.eps";

binwidth=0.25
bin(x,width)=width*floor(x/width)

set multiplot layout 2, 2
plot "../out/X.csv" using (bin(column(1),binwidth)):(1.0) smooth freq w linesp title "x_1"
plot "../out/X.csv" using (bin(column(2),binwidth)):(1.0) smooth freq w linesp title "x_2"
plot "../out/X.csv" using (bin(column(3),binwidth)):(1.0) smooth freq w linesp title "x_3"
unset multiplot

# histograms of y
# set terminal x11 5
set output "../img/histY.eps";

binwidth=0.25
bin(x,width)=width*floor(x/width)

set multiplot layout 2, 2
plot "../out/Y.csv" using (bin(column(1),binwidth)):(1.0) smooth freq w linesp title "y_1"
plot "../out/Y.csv" using (bin(column(2),binwidth)):(1.0) smooth freq w linesp title "y_2"
plot "../out/Y.csv" using (bin(column(3),binwidth)):(1.0) smooth freq w linesp title "y_3"
unset multiplot

# set terminal x11 6