// P2Comp-PTC3451 Solução da Parte Computacional da Segunda Prova de PTC3451
// Copyright (C) 2021  João Pedro de Omena Simas

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <matio.h>
#include <eigen3/Eigen/Dense>
#include <iostream>
#include <stdexcept>
#include <string>
#include <eigen3/Eigen/Eigenvalues>
#include <fstream>
#include <gsl/gsl_fft_real.h>
#include <list>

auto
readXFromMat(const std::string fname) {
  // Open .mat file
  mat_t* matFile;
  
  matFile = Mat_Open(fname.c_str(), MAT_ACC_RDONLY);
  if(matFile == nullptr)
    throw std::logic_error("Could not open .mat file.");

  std::array<std::string, 3> varNames = {"x1", "x2", "x3"};
  
  using Vec = Eigen::Matrix<double, Eigen::Dynamic, 1>;  
  std::array<Vec, 3> x;

  for(auto i = 0; i < 3; i++) {
    matvar_t* matVar = Mat_VarRead(matFile, varNames[i].c_str());

    if(matVar == nullptr || matVar->isComplex)
      throw std::logic_error("Could not find \"" + varNames[i] + "\" variable.");

    size_t xSize = matVar->nbytes/matVar->data_size;
    const double *matPtr = static_cast<const double*>(matVar->data);

    // copy data to Eigen Matrix
    Vec xi(xSize);
    memcpy(xi.data(), matPtr, matVar->nbytes);

    x[i] = xi;
    
    // free matio structs
    Mat_VarFree(matVar);
  }

  // Close file
  Mat_Close(matFile);

  Eigen::Matrix<double, 3, Eigen::Dynamic> ret(3, x[0].rows());
  ret << x[0].transpose(), x[1].transpose(), x[2].transpose();
  
  return ret;
}

template <typename T>
auto calcPeriodogram(const Eigen::Matrix<T, Eigen::Dynamic, 1>& x) {
  const size_t n = x.rows();
  Eigen::Matrix<T, Eigen::Dynamic, 1> xOut = x;
  Eigen::Matrix<T, Eigen::Dynamic, 1> xSPD = Eigen::Matrix<T, Eigen::Dynamic, 1>::Zero(n/2 + 1);

  // calcula DFT
  gsl_fft_real_wavetable* wav = gsl_fft_real_wavetable_alloc(n);
  gsl_fft_real_workspace* work = gsl_fft_real_workspace_alloc(n);

  gsl_fft_real_transform(xOut.data(), 1, n, wav, work);
  gsl_fft_real_wavetable_free(wav);
  gsl_fft_real_workspace_free(work);

  // calcula magnitude ao quadrado da DFT
  xSPD[0] = xOut[0]*xOut[0];
  for(auto i = 1; i < (n + 1)/2; i++)
    xSPD[i] = xOut[2*i - 1]*xOut[2*i - 1] + xOut[2*i]*xOut[2*i];

  if(n%2 == 0)
    xSPD[n/2] = xOut[n - 1]*xOut[n - 1];

  // divide pelo número de amostras
  xSPD /= n;
  
  return xSPD;
}

template <typename T = double>
class ex1Data {
public:
  using Vec = Eigen::Matrix<T, 3, 1>;
  using Mat = Eigen::Matrix<T, 3, 3>;

  ex1Data(const Mat& Rxx) : Rxx(Rxx) {};
protected:

  Mat Rxx;
  
  Vec eVal;
  Mat eVec;
  
  auto calcEigenPairs(const Mat& M) {
    Eigen::SelfAdjointEigenSolver<Mat> es;
    es.compute(M);
    return std::pair<Vec, Mat>(es.eigenvalues(), es.eigenvectors());
  };

public:
  void calcRxxEigenPairs() {std::tie(eVal, eVec) = calcEigenPairs(Rxx);};
  auto getRxxEigenPairs() {return std::make_pair(eVal, eVec);}

  auto runEx1();
};

template <typename T>
auto ex1Data<T>::runEx1() {
  calcRxxEigenPairs();

  std::cout << "eigenvalues:" << std::endl << eVal << std::endl;
  std::cout << "eigenvectors:" << std::endl << eVec << std::endl;
  
  //vetores de componentes principais
  auto princComps = eVec.adjoint()*eVal.array().sqrt().matrix().asDiagonal();

  //variância explicada
  auto explVar = eVal/eVal.sum();

  std::cout << "explVar = " << explVar << std::endl;
    
  // máxima SNR
  // como o ruído é branco, as componentes do ruído são iguais em
  // todas as direções, logo a máxima SNR ocorrem na direção relativa
  // ao máximo autovalor, assim:

  typename Vec::Index maxInd;
  auto max = eVal.maxCoeff(&maxInd);
  auto maxSNR = (max - 1);
  std::cout << "max SNR @ v[" << maxInd << "], SNR = " << 10*log10(maxSNR) << " dB" << std::endl;

  return std::make_tuple(eVal, eVec);
}

int
main(int argc, char* argv[]) {

  std::cout <<
    "P2Comp-PTC3451 Copyright (C) 2021  João Pedro de Omena Simas" << std::endl << 
    "This program comes with ABSOLUTELY NO WARRANTY." << std::endl << 
    "This is free software, and you are welcome to redistribute it" << std::endl <<
    "under certain conditions." << std::endl;
  
  using T = double;
  using Vec = typename ex1Data<T>::Vec;
  using Mat = typename ex1Data<T>::Mat;

  //exercício 1 a
  std::cout << "Ex 1-A" << std::endl;
  {
    const Mat Rxx = []() {
      Eigen::Matrix<T, 3, 1> rss;
      rss << 20.0, 19.39, 17.59;
      
      Eigen::Matrix<T, 3, 3> Rss;
      for(auto i = 0; i < 3; i++)
	for(auto j = 0; j < 3; j++)
	  Rss(i, j) = rss(std::abs(i - j));

      Mat Rxx = Rss + Mat::Identity();
      
      return Rxx;
    }();

    std::cout << "Rxx = " << std::endl << Rxx << std::endl;
    
    ex1Data<T> ex1a(Rxx);

    ex1a.runEx1();
  }

  //exercício 1d
  std::cout << "Ex 1-D" << std::endl;
  {
    const auto X = readXFromMat("../data/dadosAP2.mat");
    const auto Rxx = [&X]() {
      // read data from file

      Mat Rxx = Mat::Zero();
      
      for(auto i = 0; i < X.cols(); i++)
	Rxx += (X.col(i)*X.col(i).adjoint()).selfadjointView<Eigen::Upper>();

      Rxx /= X.cols();
      
      return Rxx;
    }();

    std::cout << "Rxx = " << std::endl << Rxx << std::endl;
    
    ex1Data<T> ex1d(Rxx);
    
    const auto [eVal, eVec] = ex1d.runEx1();

    decltype(X) Y = eVec.adjoint()*X;

    //salva X
    std::ofstream fileOut;
    fileOut.open("../out/X.csv");
    for(auto i = 0; i < X.cols(); i++) {
      for(auto j = 0; j < X.rows() - 1; j++)
	fileOut << X(j, i) << ", ";
      fileOut << X(X.rows() - 1, i) << std::endl;
    }
    fileOut.close();

    //salva Y
    fileOut.open("../out/Y.csv");
    for(auto i = 0; i < Y.cols(); i++) {
      for(auto j = 0; j < Y.rows() - 1; j++)
	fileOut << Y(j, i) << ", ";
      fileOut << Y(Y.rows() - 1, i) << std::endl;
    }
    fileOut.close();
  }
  
  // exercicio 2
  {
    const auto x1 = [] {
      const auto X = readXFromMat("../data/dadosAP2.mat");
      Eigen::Matrix<T, Eigen::Dynamic, 1> ret = X.row(0).transpose();
      return ret;
    }();

    std::cout << "number of samples read: " << x1.rows() << std::endl;
    
    // calcula periodograma
    const auto x1Per = calcPeriodogram(x1);

    //salva periodograma
    std::ofstream fileOut;
    fileOut.open("../out/x1Per.csv");
    for(auto i = 0; i < x1Per.rows(); i++)
      fileOut << x1Per[i] << std::endl;
    fileOut.close();

    //acha picos
    const T threshold = 10.0*x1Per.mean();
    std::list<std::pair<int, T>> peaks;

    bool started = false;
    T last = 0;
    
    for(auto i = 0; i < x1Per.rows(); i++) {
      if(started) {
	if(x1Per[i] <= last) {
	  peaks.push_back(std::make_pair(i,last));
	  started = false;
	}
      } else if(x1Per[i] > threshold) {
	started = true;
	last = x1Per[i];
      }
    }

    std::cout << "Peaks found: ";
    for(auto& pk : peaks)
      std::cout << "(" << pk.first << ", " << 10*log10(pk.second) << " dB)" << "; ";
    std::cout << std::endl;
    
  }
  
  return 0;
}
